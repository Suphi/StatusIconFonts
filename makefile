DESTDIR = /usr/share/fonts/TTF
MODE = 644

all: FontStatusIcons.ttf

install: all
		install -d ${DESTDIR}
		install -m ${MODE} FontStatusIcons.ttf ${DESTDIR}/

uninstall:
		rm -f ${DESTDIR}/FontStatusIcons.ttf

.PHONY: all install uninstall
