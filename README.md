TTF status icon fonts created with [birdfont](https://github.com/johanmattssonm/birdfont).

looks best with font sizes 10px, 20px, 30px...

#### Installation
```
make install
```
#### Removal
```
make uninstall
```

![Screenshot](https://gitlab.com/Suphi/StatusIconFonts/raw/master/Screenshot.png)
